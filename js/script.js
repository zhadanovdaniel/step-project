//our service
let ourServiceLi = document.querySelectorAll(".main_our_services_button");
let ourServiceText = document.getElementById("main_our_services_text");
let ourServiceImg = document.getElementById("main_our_services_img");
let cursor = document.querySelectorAll(".main_our_services_button_cursor");
for (
  let ourServiceLiItem = 0;
  ourServiceLiItem < ourServiceLi.length;
  ourServiceLiItem++
) {
  ourServiceLi[ourServiceLiItem].addEventListener("click", () => {
    ourServiceLi.forEach((element) => {
      element.classList.remove("services_button_active");
    });
    cursor.forEach((element) => {
      element.classList.add("deactive");
    });
    if (
      !ourServiceLi[ourServiceLiItem].classList.contains(
        "services_button_active"
      )
    ) {
      ourServiceLi[ourServiceLiItem].classList.add("services_button_active");
      ourServiceImg.src = `./img/main_switchphoto${ourServiceLiItem + 1}.png`;
      console.log(ourServiceImg.scr);
      cursor[ourServiceLiItem].classList.remove("deactive");
    }

    switch (ourServiceLiItem) {
      case 0:
        ourServiceText.textContent =
          "First ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        break;
      case 1:
        ourServiceText.textContent =
          "Second ipsum dolor sit amet, re magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        break;
      case 2:
        ourServiceText.textContent =
          "Third ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididuncepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        break;
      case 3:
        ourServiceText.textContent =
          "Firth ipsum dolor sit amet, consectetur adipisiciagna aliqua. Exat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        break;
      case 4:
        ourServiceText.textContent =
          "Fifth ipsum dolor sit amet, consectetur adipisicing elit, re magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        break;
      case 5:
        ourServiceText.textContent =
          "Sixsth ipsum dolor sit amet, consectetur adipisicing elit, re magna aliqua. Excepttat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        break;
    }
  });
}
//Our Amazing Work
let mainWorkButtons = document.querySelectorAll(".main_work_button");
let mainWorkButtonCell = document.querySelectorAll(".main_work_content_cell");

let LoadButton = document.querySelector("#load");
let ContentWrap = document.querySelector(".main_work_content_wrap");

// let childNodes = ContentWrap.childNodes;

for (
  let mainWorkButtonItem = 0;
  mainWorkButtonItem < mainWorkButtons.length;
  mainWorkButtonItem++
) {
  mainWorkButtons[mainWorkButtonItem].addEventListener("click", () => {
    mainWorkButtons.forEach((element) => {
      element.classList.remove("services_button_active");
    });
    if (
      !mainWorkButtons[mainWorkButtonItem].classList.contains(
        "services_button_active"
      )
    ) {
      mainWorkButtons[mainWorkButtonItem].classList.add(
        "services_button_active"
      );
    }
    switch (mainWorkButtonItem) {
      case 0:
        for (let j = 1; j < mainWorkButtonCell.length; j++) {
          mainWorkButtonCell[j].classList.remove("sortDeacrive");
          console.log(mainWorkButtonCell.length);
        }

        break;
      case 1:
        for (let j = 1; j < mainWorkButtonCell.length; j++) {
          mainWorkButtonCell[j].classList.add("sortDeacrive");
          console.log(mainWorkButtonCell[j]);
        }
        for (let j = 1; j < 3; j++) {
          mainWorkButtonCell[j].classList.remove("sortDeacrive");
        }
        break;
      case 2:
        for (let j = 1; j < mainWorkButtonCell.length; j++) {
          mainWorkButtonCell[j].classList.add("sortDeacrive");
        }
        for (let j = 3; j < 6; j++) {
          mainWorkButtonCell[j].classList.remove("sortDeacrive");
        }
        break;
      case 3:
        for (let j = 1; j < mainWorkButtonCell.length; j++) {
          mainWorkButtonCell[j].classList.add("sortDeacrive");
        }
        for (let j = 6; j < 8; j++) {
          mainWorkButtonCell[j].classList.remove("sortDeacrive");
        }
        break;
      case 4:
        for (let j = 1; j < mainWorkButtonCell.length; j++) {
          mainWorkButtonCell[j].classList.add("sortDeacrive");
        }
        for (let j = 8; j < 11; j++) {
          mainWorkButtonCell[j].classList.remove("sortDeacrive");
        }
        break;
    }
  });
}

LoadButton.addEventListener("click", () => {
  for (let i = 0; i < 12; i++) {
    let newLi = document.createElement("li");
    newLi.classList.add("main_work_content_cell");
    ContentWrap.append(newLi);
    let newImg = document.createElement("img");
    newImg.src = `./img/workimages/all/Layer${i + 12}.png`;
    newImg.classList.add("imgSizer");
    newLi.append(newImg);
    console.log();
  }
  LoadButton.remove();
});

//review

let arrowLeft = document.querySelector(".arrowLeft");
let user = document.querySelectorAll(".review_menu_user");
let helpCounter = 0;
let arrowRight = document.querySelector(".arrowRight");


//керування на стрілки
arrowLeft.addEventListener("click", () => {
  for (let counter = 0; counter < user.length; counter++) {
    user[counter].classList.remove("activeUser");
  }
  helpCounter = helpCounter - 1;
  if (helpCounter == -1) {
    helpCounter = 3;
  }

  user[helpCounter].classList.add("activeUser");
  switchUser();
});



arrowRight.addEventListener("click", () => {
  for (let counter = 0; counter < user.length; counter++) {
    user[counter].classList.remove("activeUser");
  }
  helpCounter = helpCounter + 1;
  if (helpCounter == 4) {
    helpCounter = 0;
  }
  
    user[helpCounter].classList.add("activeUser");
  

  switchUser();
});

let reviewText = document.querySelector(".review_comment_text");
let revieewName = document.querySelector("#name");
let reviewImg = document.querySelector(".active_image")

//керування натисканням на кружечки

for(let k=0; k<4;k++){
  user[k].addEventListener("click", ()=>{
    for (let counter = 0; counter < user.length; counter++) {
      user[counter].classList.remove("activeUser");
    }
    helpCounter = k
    
      user[helpCounter].classList.add("activeUser"); 
    
    switchUser();
  })
}



function switchUser() {
  switch (helpCounter) {
    case 0:
      reviewText.textContent =
        " First Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.";
        revieewName.textContent = "HASAN ALI";
        reviewImg.src = "./img/review/reviewFace1.png";
      break;
    case 1:
      reviewText.textContent =
        " Second Uctus, quam dui laoreet sem, non dic, augue tempus ultricies lis tum odi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non didignissimmassa. Morbiorbi puctum odio nisi quis massa. Mo nisi quInteger lvinar odio eget aliquam facilisis.";
        revieewName.textContent = "LIRA AMON";
        reviewImg.src = "./img/review/reviewFace2.png";
      break;
    case 2:
      reviewText.textContent =
        " Third pus nar odio eget aliqUctus, quam dui laoreet sem,temuam facilisis. Tempus ultricies luct non dic, augueultricies lis tum odi pulvi us, quam dui laoreet sem, non didignissimmassa. Morbiorbi puctum odio nisi quis massa. Mo nisi quInteger lvinar odio eget aliquam facilisis.";
        revieewName.textContent = "AMIR ROMASAN";
        reviewImg.src = "./img/review/reviewFace3.png";
      break;
    case 3:
      reviewText.textContent =
        " Fourth Uctus, quam dui laoreet sem, non dic, augue tempus ultricies lis tum odi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non didignissimmassa. Morbiorbi puctum odio nisi quis massa. Mo nisi quInteger lvinar odio eget aliquam facilisis.";
        revieewName.textContent = "SIRIUS BLACK";
        reviewImg.src = "./img/review/reviewFace4.png";
      break;
  }
}


